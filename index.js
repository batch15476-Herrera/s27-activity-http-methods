let http = require("http");

//Mock Data
let products = [

	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}

];


http.createServer(function(req,res){


	console.log(req.url); 
	console.log(req.method);

	if(req.url === "/products" && req.method === "GET"){

		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(products));

	} else if(req.url === "/products" && req.method === "POST"){


        let requestBody = "";

		req.on('data',function(data){

			
			requestBody += data;

		})	
		req.on('end',function(){
            console.log(requestBody)
			requestBody = JSON.parse(requestBody);

			
			let newProducts = {
				name: requestBody.name,
				price: requestBody.price,
				description: requestBody.description

			}
			products.push(newProducts);
            console.log(products)
		

			res.writeHead(201,{'Content-Type':'application/json'});
			res.end(JSON.stringify(products));

		})
    }

}).listen(8000);

console.log("Server is running at localhost:8000");  